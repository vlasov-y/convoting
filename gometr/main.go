package main

import (
	"net/http"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	params, ok := r.URL.Query()["action"]
	if !ok || len(params[0]) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	switch params[0] {
		case "yes": yes.Inc()
		case "no": no.Inc()
		case "cool": cool.Inc()
		case "missed": missed.Inc()
		default: {
			w.WriteHeader(http.StatusBadRequest)
		}
			
	}
}

var (
	yes = promauto.NewCounter(prometheus.CounterOpts{
		Name: "gometr_yes",
		Help: "Voted positive",
	})
	no = promauto.NewCounter(prometheus.CounterOpts{
		Name: "gometr_no",
		Help: "Voted negative",
	})
	cool = promauto.NewCounter(prometheus.CounterOpts{
		Name: "gometr_cool",
		Help: "Confirmed that material is cool! Continue!",
	})
	missed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "gometr_missed",
		Help: "Do not understood what is happening! Repeat...",
	})
)

func main() {
	// recordMetrics()
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/push", rootHandler)
	http.ListenAndServe(":2112", nil)
}