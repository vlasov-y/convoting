Convoting
===
- [Convoting](#convoting)
  - [Overview](#overview)
    - [Architecture](#architecture)
    - [Run](#run)

## Overview
Can be used during presentation or online-conference in order to gather feedback from auditory.

### Architecture
```mermaid
graph TB
  Nginx --> Site[Site content]
  Nginx --> Grafana
  Nginx --> GoMetr
  Grafana --> Prometheus
  GoMetr --> Prometheus
```

### Run
```shell
docker-compose up -d
```
And open [localhost](http://localhost) in browser.